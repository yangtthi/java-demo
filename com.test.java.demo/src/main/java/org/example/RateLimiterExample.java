package org.example;

import com.google.common.util.concurrent.RateLimiter;

public class RateLimiterExample {
    public static void main(String[] args) {
        // 创建一个每秒允许2个许可证的RateLimiter
        RateLimiter rateLimiter = RateLimiter.create(2);

        for (int i = 0; i < 10; i++) {
            // 获取一个许可证，如果没有可用的许可证，此方法会阻塞直到有一个可用
            rateLimiter.acquire();
            System.out.println("执行任务： " + i);
        }
    }
}
