package org.example.shoplazza;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @Description: TODO
 * @Author wyatt
 * @Data 2024/07/11 18:31
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AppInfo {

    private String id;
    private String listing_name;
    private String subtitle;
    private String overall_rating;
    private String review_total;
    private List<Tag> tags;
    private String tagNames;
    private String price_tag;
    private String charge_type;
    private String min_price;
    private String launched;
    private String developer;
    private String areas;
    private String language;
    private String category ;


    @Data
    class Tag {
        private String name_zh;

    }
}
