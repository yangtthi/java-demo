package org.example.analyse;

import com.hankcs.hanlp.HanLP;
import com.hankcs.hanlp.seg.common.Term;

import java.util.List;

/**
 * @Description: TODO
 * @Author wyatt
 * @Data 2024/09/29 15:43
 */
public class HanLPUtil {

    public static void main(String[] args) {
        String text = "我不便于回答这个问题。Shopify解决了哪些问题以及这对您有什么好处？管理更轻松了。";
        List<Term> termList = HanLP.segment(text);
        System.out.println(termList);
    }

}
