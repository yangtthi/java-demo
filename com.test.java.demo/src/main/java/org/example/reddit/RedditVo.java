package org.example.reddit;

import lombok.Data;

import java.io.Serializable;

/**
 * @Description: TODO
 * @Author wyatt
 * @Data 2024/09/25 15:18
 */
@Data
public class RedditVo implements Serializable {

    private String platform;

    private String createdAt;

    private String content;

    private String translate;

    private String analysis;

    private String evaluate;

    private String classify;


}
