package org.example.xf;

import lombok.Data;

import java.util.List;

/**
 * @Description: TODO
 * @Author wyatt
 * @Data 2024/07/11 20:09
 */
@Data
public class ChatRep {

    private String sid;
    private List<Choice> choices;

    public class Choice {
        private Message message;

        public Choice(Message message) {
            this.message = message;
        }

        public Message getMessage() {
            return message;
        }

        public void setMessage(Message message) {
            this.message = message;
        }
    }

    public class Message {
        private String content;

        public Message(String content) {
            this.content = content;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }
    }

}



