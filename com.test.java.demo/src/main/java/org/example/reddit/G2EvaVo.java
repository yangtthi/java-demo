package org.example.reddit;

import lombok.Data;

import java.io.Serializable;

/**
 * @Description: TODO
 * @Author wyatt
 * @Data 2024/09/26 10:06
 */
@Data
public class G2EvaVo implements Serializable {

    private String platform;
    private String content;
    private String goal;

    private String translate;

    private String title;
    private String likeReason;

    private String disLikeReason;

    private String likeReasonTranslate;

    private String likeReasonClassify;

    private String disLikeReasonTranslate;

    private String disLikeReasonClassify;

}
