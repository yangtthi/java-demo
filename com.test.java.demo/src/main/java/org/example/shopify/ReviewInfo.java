package org.example.shopify;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Description: TODO
 * @Author wyatt
 * @Data 2024/07/11 18:31
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ReviewInfo {

    private String appName;
    private String reviewerName;
    private String reviewerAddress;
    private String reviewerUseTime;
    private String stars;
    private String reviewInfo;
    private String reviewInfoTranslate;
    private String reviewInfoTag;
    private String reviewUpdateTime;
    private String answer;
    private String answerTranslate;
    private String answerTime;

}
