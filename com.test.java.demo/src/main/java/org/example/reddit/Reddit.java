package org.example.reddit;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.io.FileUtil;
import cn.hutool.core.io.file.FileReader;
import cn.hutool.core.io.resource.ResourceUtil;
import cn.hutool.core.lang.Console;
import cn.hutool.core.text.csv.CsvData;
import cn.hutool.core.text.csv.CsvReader;
import cn.hutool.core.text.csv.CsvRow;
import cn.hutool.core.text.csv.CsvUtil;
import cn.hutool.poi.excel.ExcelUtil;
import cn.hutool.poi.excel.ExcelWriter;
import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.example.xf.ChatUtil;

import java.io.File;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @Description: Reddit
 * @Author wyatt
 * @Data 2024/09/25 15:11
 */
@Slf4j
public class Reddit {

    public static void main(String[] args) {
        /**
         * 1. 查询目录文件
         * 2. 读取文件，统计
         * 3. 输入到单个表格
         * 4. 增加模型分析：翻译、评论简析、好评/差评/中评、分类  好评还是差评
        */

        File[] files = FileUtil.ls("D:\\Desktop\\reddit");

        String excelName = "D:\\Desktop\\shopify\\reddit_20240925_6" + ".xlsx";

        List<RedditVo> rows = new ArrayList<>();
        int count = 0;
        for (File file : files) {
            System.out.println(file.getName().substring(0, file.getName().lastIndexOf(".csv")));
            System.out.println(file.getPath());
            FileReader fileReader = new FileReader(file.getPath());
            List<String> csvDateList = fileReader.readLines();
            count = 0;
            for (String csvData : csvDateList) {

               try{
                   RedditVo redditVo = new RedditVo();
                   redditVo.setPlatform(file.getName().substring(0, file.getName().lastIndexOf(".csv")));
                   ZonedDateTime zonedDateTime = ZonedDateTime.parse(csvData.substring(0,24), DateTimeFormatter.ISO_ZONED_DATE_TIME);
                   redditVo.setCreatedAt(DateUtil.formatDateTime(Date.from(zonedDateTime.toInstant())));
                   redditVo.setContent(csvData.substring(25));

                   try{
                       redditVo.setTranslate(ChatUtil.chatTranslate(redditVo.getContent()));
//                       if(count < 10){
//                           redditVo.setTranslate(ChatUtil.chatTranslate(redditVo.getContent()));
//                           redditVo.setAnalysis(ChatUtil.chatTranslateSimple(redditVo.getContent()));
//                           redditVo.setEvaluate(ChatUtil.chatEvaluate(redditVo.getContent()));
//                           redditVo.setClassify(ChatUtil.chatClassify(redditVo.getContent()));
//                       }

                   }catch (Exception e){
                       e.getStackTrace();
                       System.out.println("analysis error : " + csvData);
                   }
                   System.out.println(JSON.toJSONString(redditVo));
                   rows.add(redditVo);
               }catch (Exception e){
                   e.getStackTrace();
                   System.out.println("error : " + csvData);
               }
               count++;
               if(count % 10 == 0){
                   System.out.println("count: " + count);
                   System.out.println("count: " + count);
                   System.out.println("count: " + count);
                   System.out.println("count: " + count);
                   System.out.println("count: " + count);
               }
            }

//            break;
        }

        // 通过工具类创建writer
        ExcelWriter writer = ExcelUtil.getWriter(excelName);
        // 一次性写出内容，使用默认样式，强制输出标题
        writer.write(rows, true);
        // 关闭writer，释放内存
        writer.close();
        System.out.println("导出完成" + excelName + " size: "+ rows.size());
    }
}
