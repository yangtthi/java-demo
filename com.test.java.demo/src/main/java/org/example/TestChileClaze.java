package org.example;

import lombok.Data;

/**
 * @Description: TODO
 * @Author wyatt
 * @Data 2024/06/26 10:35
 */

@Data
public class TestChileClaze extends TestParentClaze {

    private String age;

    public String getAge() {
        System.out.println(this.getClass().getName() + " get Age " + age);
        return age;
    }

    public void setAge(String age) {
        System.out.println(this.getClass().getName() + " set Age " + age);
        this.age = age;
    }
}
