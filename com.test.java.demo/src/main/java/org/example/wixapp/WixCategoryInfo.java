package org.example.wixapp;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @Description: TODO
 * @Author wyatt
 * @Data 2024/10/21 14:06
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Deprecated
public class WixCategoryInfo {

    List<CategoryToSubCategory> categoryToSubCategory;

    @Data
    class CategoryToSubCategory {
        private Category category;
        private List<SubCategory> subCategories;
    }
    @Data
    class Category {
        private String name;
        private String slug;
    }

    @Data
    class SubCategory {
        private String name;
        private String slug;
        private List<App> apps;
    }

    @Data
    class App {
        private String appId;
    }
}
