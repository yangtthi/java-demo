package org.example.reddit;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.io.file.FileReader;
import cn.hutool.poi.excel.ExcelUtil;
import cn.hutool.poi.excel.ExcelWriter;
import com.alibaba.fastjson.JSON;
import com.wechat.pay.java.core.http.UrlEncoder;
import lombok.extern.slf4j.Slf4j;
import org.example.xf.ChatUtil;
import org.example.xf.WmChatUtil;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * @Description: Reddit
 * @Author wyatt
 * @Data 2024/09/25 15:11
 */
@Slf4j
public class GoEva {

    public static void main(String[] args) {
        /**
         * 1. 查询目录文件
         * 2. 读取文件，统计
         * 3. 输入到单个表格
         * 4. 增加模型分析：翻译、评论简析、好评/差评/中评、分类  好评还是差评
        */

        File[] files = FileUtil.ls("D:\\Desktop\\g2");

        String excelName = "D:\\Desktop\\shopify\\g2_20241011_12" + ".xlsx";

        List<G2EvaVo> rows = new ArrayList<>();
        int count = 0;
        for (File file : files) {
            System.out.println(file.getName().substring(0, file.getName().lastIndexOf(".csv")));
            System.out.println(file.getPath());
            FileReader fileReader = new FileReader(file.getPath());
            List<String> csvDateList = fileReader.readLines();
//            count = 0;
            for (String csvData : csvDateList) {

               try{
                   G2EvaVo g2EvaVo = new G2EvaVo();
                   g2EvaVo.setPlatform(file.getName().substring(0, file.getName().lastIndexOf(".csv")));
                   g2EvaVo.setContent(csvData);
                   try{
                       if(g2EvaVo.getPlatform().equals("g2-shopify")){
                           String goal = csvData.substring(0,8);
                           String title = csvData.substring(7).split("What do you like best about Shopify?")[0];
                           String likeReason = csvData.substring(7).split("What do you like best about Shopify?")[1].split("What do you dislike about Shopify?")[0];
                           String disLikeReason = csvData.substring(7).split("What do you like best about Shopify?")[1].split("What do you dislike about Shopify?")[1];

                           g2EvaVo.setGoal(goal);
                           g2EvaVo.setTitle(title);
                           g2EvaVo.setLikeReason(likeReason);
                           g2EvaVo.setDisLikeReason(disLikeReason);

                           if(g2EvaVo.getLikeReason().startsWith("?")){
                               g2EvaVo.setLikeReason(g2EvaVo.getLikeReason().substring(1));
                           }

                           if(g2EvaVo.getDisLikeReason().startsWith("?")){
                               g2EvaVo.setDisLikeReason(g2EvaVo.getDisLikeReason().substring(1));
                           }

                           g2EvaVo.setLikeReasonTranslate(WmChatUtil.chatTranslate(g2EvaVo.getLikeReason()));
                           g2EvaVo.setDisLikeReasonTranslate(WmChatUtil.chatTranslate(g2EvaVo.getDisLikeReason()));

                           g2EvaVo.setLikeReasonClassify(WmChatUtil.chatClassify(UrlEncoder.urlEncode(g2EvaVo.getLikeReason()), 1));
                           g2EvaVo.setDisLikeReasonClassify(WmChatUtil.chatClassify(UrlEncoder.urlEncode(g2EvaVo.getDisLikeReason()), 1));

                       } else if(g2EvaVo.getPlatform().equals("g2-wix")) {
                           String goal = csvData.substring(0,8);
                           String title = csvData.substring(8).split("What do you like best about Wix?")[0];
                           String likeReason = csvData.substring(8).split("What do you like best about Wix?")[1].split("What do you dislike about Wix?")[0];
                           String disLikeReason = csvData.substring(8).split("What do you like best about Wix?")[1].split("What do you dislike about Wix?")[1];

                           g2EvaVo.setGoal(goal);
                           g2EvaVo.setTitle(title);
                           g2EvaVo.setLikeReason(likeReason);
                           g2EvaVo.setDisLikeReason(disLikeReason);

                           if(g2EvaVo.getLikeReason().startsWith("?")){
                               g2EvaVo.setLikeReason(g2EvaVo.getLikeReason().substring(1));
                           }

                           if(g2EvaVo.getDisLikeReason().startsWith("?")){
                               g2EvaVo.setDisLikeReason(g2EvaVo.getDisLikeReason().substring(1));
                           }

                           g2EvaVo.setLikeReasonTranslate(WmChatUtil.chatTranslate(g2EvaVo.getLikeReason()));
                           g2EvaVo.setDisLikeReasonTranslate(WmChatUtil.chatTranslate(g2EvaVo.getDisLikeReason()));

                           g2EvaVo.setLikeReasonClassify(WmChatUtil.chatClassify(UrlEncoder.urlEncode(g2EvaVo.getLikeReason()), 2));
                           g2EvaVo.setDisLikeReasonClassify(WmChatUtil.chatClassify(UrlEncoder.urlEncode(g2EvaVo.getDisLikeReason()), 2));

                       } else if(g2EvaVo.getPlatform().equals("g2-woocommerce")) {
                           String goal = csvData.substring(0,8);
                           String title = csvData.substring(8).split("What do you like best about WooCommerce??")[0];
                           String likeReason = csvData.substring(8).split("What do you like best about WooCommerce?")[1].split("What do you dislike about WooCommerce?")[0];
                           String disLikeReason = csvData.substring(8).split("What do you like best about WooCommerce?")[1].split("What do you dislike about WooCommerce?")[1];

                           g2EvaVo.setGoal(goal);
                           g2EvaVo.setTitle(title);
                           g2EvaVo.setLikeReason(likeReason);
                           g2EvaVo.setDisLikeReason(disLikeReason);

                           if(g2EvaVo.getLikeReason().startsWith("?")){
                               g2EvaVo.setLikeReason(g2EvaVo.getLikeReason().substring(1));
                           }

                           if(g2EvaVo.getDisLikeReason().startsWith("?")){
                               g2EvaVo.setDisLikeReason(g2EvaVo.getDisLikeReason().substring(1));
                           }

                           g2EvaVo.setLikeReasonTranslate(WmChatUtil.chatTranslate(g2EvaVo.getLikeReason()));
                           g2EvaVo.setDisLikeReasonTranslate(WmChatUtil.chatTranslate(g2EvaVo.getDisLikeReason()));

                           g2EvaVo.setLikeReasonClassify(WmChatUtil.chatClassify(UrlEncoder.urlEncode(g2EvaVo.getLikeReason()), 3));
                           g2EvaVo.setDisLikeReasonClassify(WmChatUtil.chatClassify(UrlEncoder.urlEncode(g2EvaVo.getDisLikeReason()), 3));


                       }

                       if(g2EvaVo.getGoal().contains("stars-")){
                           g2EvaVo.setGoal(g2EvaVo.getGoal().replace("stars-", ""));
                       }

                       if(g2EvaVo.getTitle().length() > 2){
                           g2EvaVo.setTitle(g2EvaVo.getTitle().substring(2, g2EvaVo.getTitle().length()-2));
                       }


                   }catch (Exception e){
                       e.getStackTrace();
                       System.out.println("analysis error : " + csvData);
                   }

//                   if(count > 100) break;
//
                   System.out.println(JSON.toJSONString(g2EvaVo));
                   rows.add(g2EvaVo);

               }catch (Exception e){
                   e.getStackTrace();
                   System.out.println("error : " + csvData);
               }
               count++;
               if(count % 10 == 0){
                   System.out.println("count: " + count);
                   System.out.println("count: " + count);
                   System.out.println("count: " + count);
                   System.out.println("count: " + count);
                   System.out.println("count: " + count);
               }
            }

//            break;
        }

        // 通过工具类创建writer
        ExcelWriter writer = ExcelUtil.getWriter(excelName);
        // 一次性写出内容，使用默认样式，强制输出标题
        writer.write(rows, true);
        // 关闭writer，释放内存
        writer.close();
        System.out.println("导出完成" + excelName + " size: "+ rows.size());
    }
}
