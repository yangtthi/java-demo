package org.example;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class Main {
    public static void main(String[] args) {


        String dateTimeStr = "2024-09-24T01:03:55.124Z";
        DateTimeFormatter formatter = DateTimeFormatter.ISO_ZONED_DATE_TIME;
        ZonedDateTime zonedDateTime = ZonedDateTime.parse(dateTimeStr, formatter);
        System.out.println(zonedDateTime);
        System.out.println(Date.from(zonedDateTime.toInstant()));



//        TestChileClaze chileClaze1 = new TestChileClaze();
//        chileClaze1.setAge("1");
//        chileClaze1.getAge();
//
//        TestParentClaze parentClaze2 =  new TestParentClaze();
//        parentClaze2.setAge("2");
//        parentClaze2.getAge();
//
//        TestParentClaze parentClaze3 = new TestChileClaze();
//
//        parentClaze3.setAge("3");
//        parentClaze3.getAge();
//
//        System.out.println("Hello world!");
    }
}