//package org.example.analyse;
//
//import edu.mit.jwi.Dictionary;
//import edu.mit.jwi.IDictionary;
//import edu.mit.jwi.item.IIndexWord;
//import edu.mit.jwi.item.ISynset;
//import edu.mit.jwi.item.IWord;
//import edu.mit.jwi.item.Pointer;
//import edu.mit.jwi.item.POS;
//import edu.mit.jwi.morph.WordnetStemmer;
//
//import java.io.File;
//import java.io.IOException;
//import java.net.URL;
//import java.util.ArrayList;
//import java.util.List;
//
//public class FindSynonyms {
//    public static void main(String[] args) throws IOException {
//        // 设置WordNet数据库的路径
//        String wnhome = "C:\\wordnet\\dict";
//        String path = wnhome + File.separator + "index.noun";
//        URL url = new URL("file", null, path);
//
//        // 创建字典对象
//        IDictionary dict = new Dictionary(url);
//        dict.open();
//
//        // 输入要查找同义词的单词
//        String word = "happy";
//
//        // 获取单词的所有同义词集
//        List<ISynset> synsets = getSynsets(dict, word);
//
//        // 输出同义词集及其包含的单词
//        for (ISynset synset : synsets) {
//            System.out.println("Synset ID: " + synset.getID());
//            for (IWord w : synset.getWords()) {
//                System.out.println("Word: " + w.getLemma());
//            }
//            System.out.println();
//        }
//
//        // 关闭字典
//        dict.close();
//    }
//
//    private static List<ISynset> getSynsets(IDictionary dict, String word) {
//        List<ISynset> synsets = new ArrayList<>();
//        IIndexWord idxWord = dict.getIndexWord(word, POS.NOUN);
//        if (idxWord != null) {
//            for (IWordID wordID : idxWord.getWordIDs()) {
//                ISynset synset = dict.getSynset(wordID);
//                synsets.add(synset);
//            }
//        }
//        return synsets;
//    }
//}
