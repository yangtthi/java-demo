package org.example.wixapp;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @Description: TODO
 * @Author wyatt
 * @Data 2024/07/11 18:31
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class WixAppInfo {

    private String parentCategory;
    private String parentCategoryName;
    private String subCategory;
    private String subCategoryName;
    private String appId;
    private String appSlug;
    private String appName;
    private String shortDescription;
    private String createDate;
    private String companyName;
    private String companyWebsite;

}
