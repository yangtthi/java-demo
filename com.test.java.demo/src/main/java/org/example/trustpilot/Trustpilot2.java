package org.example.trustpilot;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.io.FileUtil;
import cn.hutool.core.io.file.FileReader;
import cn.hutool.poi.excel.ExcelUtil;
import cn.hutool.poi.excel.ExcelWriter;
import com.alibaba.fastjson.JSON;
import com.wechat.pay.java.core.http.UrlEncoder;
import lombok.extern.slf4j.Slf4j;
import org.example.reddit.G2EvaVo;
import org.example.xf.WmChatUtil;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * @Description: Trustpilot
 * @Author wyatt
 * @Data 2024/09/25 15:11
 */
@Slf4j
public class Trustpilot2 {

    public static void main(String[] args) {
        /**
         * 1. 查询目录文件
         * 2. 读取文件，统计
         * 3. 输入到单个表格
         * 4. 增加模型分析：翻译、评论简析、好评/差评/中评、分类  好评还是差评
        */

        File[] files = FileUtil.ls("D:\\Desktop\\trustpilot");

        String excelName = "D:\\Desktop\\shopify\\trust_pilot_20240929_16" + ".xlsx";

        List<G2EvaVo> rows = new ArrayList<>();
        int count = 0;
        int len = 0;
        for (File file : files) {
            System.out.println(file.getName().substring(0, file.getName().lastIndexOf(".csv")));
            System.out.println(file.getPath());
            FileReader fileReader = new FileReader(file.getPath());
            List<String> csvDateList = fileReader.readLines();
            count = 0;
            for (String csvData : csvDateList) {

               try{
                   G2EvaVo g2EvaVo = new G2EvaVo();
                   g2EvaVo.setPlatform(file.getName().substring(0, file.getName().lastIndexOf(".csv")));
                   g2EvaVo.setContent(csvData);
                   try{
                       g2EvaVo.setGoal(csvData.substring(0,1));
                       g2EvaVo.setContent(csvData.substring(2));
                       if(count < 200000){
                           String msg = WmChatUtil.chatClassify(UrlEncoder.urlEncode(g2EvaVo.getContent()), 0);
                           if(msg.contains(" ")){
                               String[] arr = msg.split(" ");
                               g2EvaVo.setTranslate(arr[0]);
                               for (int i = 1; i < arr.length; i++) {
                                   if(arr[i].length() < 4){
                                       continue;
                                   }
                                   G2EvaVo g2EvaVo2 = new G2EvaVo();
                                   BeanUtil.copyProperties(g2EvaVo, g2EvaVo2);
                                   g2EvaVo2.setTranslate(arr[i]);
                                   rows.add(g2EvaVo2);
                               }
                           }else {
                               g2EvaVo.setTranslate(msg);
                           }

                       }
                       len+=g2EvaVo.getContent().length();
                   }catch (Exception e){
                       e.getStackTrace();
                       System.out.println("analysis error : " + csvData);
                   }

                   System.out.println(JSON.toJSONString(g2EvaVo));
                   rows.add(g2EvaVo);

               }catch (Exception e){
                   e.getStackTrace();
                   System.out.println("error : " + csvData);
               }
               count++;
               if(count % 10 == 0){
                   System.out.println("count: " + count);
                   System.out.println("count: " + count);
                   System.out.println("count: " + count);
                   System.out.println("count: " + count);
                   System.out.println("count: " + count);
               }
            }

//            break;
        }

        // 通过工具类创建writer
        ExcelWriter writer = ExcelUtil.getWriter(excelName);
        // 一次性写出内容，使用默认样式，强制输出标题
        writer.write(rows, true);
        // 关闭writer，释放内存
        writer.close();
        System.out.println(len);
        System.out.println("导出完成" + excelName + " size: "+ rows.size());
    }
}
