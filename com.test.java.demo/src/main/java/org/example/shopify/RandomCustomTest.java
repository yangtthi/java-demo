package org.example.shopify;

import cn.hutool.poi.excel.ExcelUtil;
import cn.hutool.poi.excel.ExcelWriter;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * @Description: ReviewTest
 * @Author wyatt
 * @Data 2024/07/11 18:29
 */
@Slf4j
public class RandomCustomTest {

    public static void main(String[] args) {
        String app = "custom";
        String excelName = "D:\\Desktop\\shopify\\20240712008_" + app  + ".xlsx";
        exportExcel(excelName);
    }

    private static void exportExcel(String excelName) {

        List<ReviewInfo> rows = new ArrayList<>();

        for (int i = 0; i < 10000; i++) {
            String last_name = generateRandomString(8);
            String family_name = generateRandomString(4);

            log.info("{}, {}, {}", last_name, family_name, last_name + "." + family_name + "@test.com");
            ReviewInfo review = new ReviewInfo();

            review.setAppName(last_name);
            review.setReviewerName(family_name);
            review.setReviewerAddress(last_name + "." + family_name + "@test.com");
            rows.add(review);
        }

        // 通过工具类创建writer
        ExcelWriter writer = ExcelUtil.getWriter(excelName);
        // 一次性写出内容，使用默认样式，强制输出标题
        writer.write(rows, true);
        // 关闭writer，释放内存
        writer.close();
        System.out.println("导出完成" + excelName + " size: "+ rows.size());
    }

    public static String generateRandomString(int length) {
        String characters = "abcdefghijklmnopqrstuvwxyz";
        Random random = new Random();
        StringBuilder stringBuilder = new StringBuilder(length);

        for (int i = 0; i < length; i++) {
            int index = random.nextInt(characters.length());
            stringBuilder.append(characters.charAt(index));
        }

        return stringBuilder.toString();
    }

}
